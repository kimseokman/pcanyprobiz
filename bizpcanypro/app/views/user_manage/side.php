<?
/**
 * html / wrap / container / side
 */
?>
<!-- container -->
<div id="container">
	<!-- side -->
	<div class="side">
		<h3 class="title" title="사용자 관리"><i class="fa fa-users"></i></h3>
		<!-- group_menu -->
		<div class="btn-group group_menu">
			<a href="#" class="btn btn-default btn-xs add_user_btn" data-toggle="modal" data-target=".create_user">
				<i class="fa fa-plus"></i>
			</a>
			<a href="#" class="btn btn-default btn-xs del_user_btn" data-toggle="modal" data-target=".delete_user">
				<i class="fa fa-minus"></i>
			</a>
		</div>
		<!-- //group_menu -->
		<!-- group_area -->
		<div class="group_area">
			<ul class="tree">
				<li class="admin">
					<a href="#">
						<span class="side_name">
							<i class="fa fa-user"></i> <? echo $admin->id; ?>
						</span>
					</a>
					<input type="hidden" class="admin_index" value="<? echo $admin->index; ?>" />
				</li>
<?
	foreach($belong_user as $user){
?>
				<li>
					<ul class="user_list droppable">
						<li class="user">
							<a href="#">
								<span class="side_name">
									<i class="
<?
		if(get_cookie($user->index)=="close"){
			echo "fa fa-caret-right";
		}else{
			echo "fa fa-caret-down";
		}
?>
									"></i> <i class="fa fa-user"></i> <? echo $user->id; ?>
								</span>
							</a>
							<input type="hidden" class="user_id" name="user_id" value="<? echo $user->id; ?>" />
							<input type="hidden" class="user_index" value="<? echo $user->index; ?>" />
							<input type="hidden" class="isFold" name="isFold" value=
<?
		if(get_cookie($user->index)=="close"){
			echo "'true'";
		}else{
			echo "'false'";
		}
?>
							/>
						</li>
<?
		$user_group_data = array('u_index' =>$user->index);
		$temp['user_group'] = $this->pcany_group_model->getGroup($user_group_data);

		foreach($temp['user_group'] as $group){
			$user_connect_data = array('g_index' => $group->index);
			$temp['user_group_connect'] = $this->pcany_group_pc_connect_model->getConnect($user_connect_data);

			foreach($temp['user_group_connect'] as $user_pc_connect){
				$user_pc_data = array('servernum' => $user_pc_connect->p_index);		//pc data setting
				$user_pc = $this->neturo_server_info_model->getPcOnce($user_pc_data);				//pc data get
?>
						<li class="auth_pc" style='
<?
				if(get_cookie($user->index) == "close"){
					echo "display:none;";
				}
?>
						'>
							
							<a href="#">
								<span class="side_name">
									<i class="fa fa-desktop"></i> <? echo $user_pc->com_name; ?>
								</span>
							</a>
							<input type="hidden" class="auth_pc_name" name="auth_pc_name" value="<? echo $user_pc->com_name; ?>" />
							<input type="hidden" class="auth_pc_index" value="<? echo $user_pc->servernum; ?>" />
							<input type="hidden" class="auth_user_index" value="<? echo $user->index; ?>" />
							<input type="hidden" class="auth_user_id" value="<? echo $user->id; ?>" />
						</li>
<?
			}//end of foreach($temp['user_group_connect'])
		}//end of foreach($temp['user_group'])
?>
					</ul>
				</li>
<?
	}//end of foreach($belong_user as $user)
?>
			</ul>
		</div>
		<!-- //group_area -->
	</div>
	<!-- //side -->


<!-- right click menu -->
<ul class="dropdown-menu side_drop_menu" role="menu" aria-labelledby="dLabel" id="admin_menu_user" style="display:none;position:relative; z-index:100;">
	<li>
		<a class="user_add" href="#">
			<i class="fa fa-plus"></i> 사용자 추가
		</a>
	</li>
	<li>
		<a class="user_del" href="#">
			<i class="fa fa-minus"></i> 사용자 삭제
		</a>
	</li>
</ul>

<ul class="dropdown-menu side_drop_menu" role="menu" aria-labelledby="dLabel" id="admin_menu_pc" style="display:none;position:relative; z-index:100;">
	<li>
		<a class="pc_del" href="#">
			<i class="fa fa-minus"></i> PC 삭제
		</a>
	</li>
</ul>
<!-- //right click menu -->

<!-- modal window -->
<?
/**
 * 사용자 생성 팝업
 */
?>
<div class="modal fade create_user" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form class="form-horizontal" method="post" role="form" action="<?echo site_url('user_manage/create_user')?>">
			<div class="modal-header header_primary">
				사용자 생성
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div class="alert_header">
					<span class="label_primary">
						<i class="fa fa-user"></i> NEW
					</span>
				</div>
				<div class="alert_content">
					<fieldset>
						<legend class="sr-only">사용자 생성</legend>
						<p class="alert_msg">
							<label class="user_id create_label" for="user_id"> ID </label>
							<input type="text" class="form-control input-sm create_input" placeholder="ID" id="user_id" name="user_id"/>
						</p>
						<p class="alert_msg">
							<label class="user_pw create_label" for="user_pw"> PW </label>
							<input type="password" class="form-control input-sm create_input" placeholder="PASSWORD" id="user_pw" name="user_pw"/>
						</p>
						<p class="alert_msg">
							<label class="user_pw_confirm create_label" for="user_confirm"> PW 확인 </label>
							<input type="password" class="form-control input-sm create_input" placeholder="PASSWORD Confirm" name="user_pw_confirm" id="user_confirm"/>
						</p>
						<p class="alert_msg">
							<label class="user_name create_label" for="user_name"> 이름 </label>
							<input type="text" class="form-control input-sm create_input" placeholder="NAME" name="user_name" id="user_name" />
						</p>
					</fieldset>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-sm confirm_btn" title="사용자 생성">
					생성
				</button>
				<button type="button" class="btn btn-default btn-sm cancel_btn"  data-dismiss="modal" title="취소">
					취소
				</button>
			</div>
			</form>
		</div>
	</div>
</div>
<?
/**
 * 사용자 삭제 팝업
 */
?>
<div class="modal fade delete_user" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
		<form class="form-horizontal" method="post" role="form" action="<?echo site_url('user_manage/delete_user')?>">
			<fieldset>
				<div class="modal-header header_danger">
					사용자 삭제
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<input type="hidden" id="select_user" name="select_user" value="" />
					<div class="alert_header">
						<span class="label_danger">
							 <i class="fa fa-user"></i> <span class="select_user_id"></span>
						</span>
					</div>
					<div class="alert_content">
						<p class="alert_msg">
							<span class="text-danger">삭제</span> 하시겠습니까?
						</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-sm confirm_btn" title="사용자 삭제">
						삭제
					</button>
					<button type="button" class="btn btn-default btn-sm cancel_btn"  data-dismiss="modal" title="취소">
						취소
					</button>
				</div>
			</fieldset>
		</form>
		</div>
	</div>
</div>
<?
/**
 * PC 제거 팝업
 */
?>
<div class="modal fade remove_pc" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
		<form class="form-horizontal" method="post" role="form" action="<?echo site_url('user_manage/remove_pc')?>">
			<fieldset>
			<div class="modal-header header_warning">
				PC 권한 해제
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="belong_to_user" name="belong_to_user" value="" />
				<input type="hidden" id="select_pc_index" name="select_pc_index" value="" />
				<div class="alert_header">
					<span class="label_warning">
						<i class="fa fa-desktop"></i><span class="sel_pc_name"></span> 
					</span>에 대한
				</div>
				<div class="alert_content">
					<p class="alert_msg">
						<span class="text-warning">
							<i class="fa fa-user"></i> <span class="auth_user_id"></span>
						</span>의 권한을 해제 하시겠습니까?
					</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-warning btn-sm confirm_btn" title="권한 해제">
					해제
				</button>
				<button type="button" class="btn btn-default btn-sm cancel_btn"  data-dismiss="modal" title="취소">
					취소
				</button>
			</div>
			</fieldset>
		</form>
		</div>
	</div>
</div>
<?
/**
 * PC 복사 팝업
 */
?>
<div class="modal fade copy_pc" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
		<form class="form-horizontal" method="post" role="form" action="<?echo site_url('user_manage/copy_pc')?>">
			<fieldset>
			<legend class="sr-only">원격대상 복사</legend>
				<div class="modal-header header_success">
					PC 권한 할당
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<input type="hidden" class="bef_user" name="bef_user"/>
					<input type="hidden" class="sel_pc" name="sel_pc"/>
					<input type="hidden" class="aft_user" name="aft_user"/>
					<div class="alert_header">
						<span class="label_success">
							<i class="fa fa-desktop"></i><span class="sel_pc_name"></span>
						</span>의 권한을
					</div>
					<div class="alert_content">
						<p class="alert_msg">
							<span class="text-success">
								<i class="fa fa-user"></i> <span class="aft_user_name"></span> 
							</span> 에게 할당 하시겠습니까?
						</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success btn-sm confirm_btn" title="권한 할당">
						할당
					</button>
					<button type="button" class="btn btn-default btn-sm cancel_btn"  data-dismiss="modal" title="취소">
						취소
					</button>
				</div>
			</fieldset>
		</form>
		</div>
	</div>
</div>


