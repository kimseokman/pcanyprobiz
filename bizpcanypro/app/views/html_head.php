<?
/**
 * html Header
 * Common
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><? echo $file_name; ?> | <? echo $title; ?></title>
	<!-- CSS -->
	<link rel="stylesheet" href="<? echo base_url(); ?>/static/lib/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<? echo base_url(); ?>/static/lib/bootstrap/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="<? echo base_url(); ?>/static/lib/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<? echo base_url(); ?>/static/lib/jquery/ui/css/jquery-ui-1.10.4.custom.min.css">
	<link rel="stylesheet" href="<? echo base_url(); ?>/static/css/common.css" />
	<link rel="stylesheet" href="<? echo base_url(); ?>/static/css/header.css" />
	<link rel="stylesheet" href="<? echo base_url(); ?>/static/css/<? echo $file_name; ?>_container.css" />
	<!-- Script -->
	<script src="<? echo base_url(); ?>/static/lib/jquery/jquery-1.11.1.min.js"></script>
	<script src="<? echo base_url(); ?>/static/lib/jquery/ui/js/jquery-ui-1.10.4.custom.min.js"></script>
	<script src="<? echo base_url(); ?>/static/js/jquery.cookie.js"></script>
</head>
<body>
<!-- wrap -->
<div id="wrap">