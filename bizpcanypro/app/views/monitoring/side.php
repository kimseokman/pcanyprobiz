<?
/**
 * html / wrap / container / side
 */
?>
<!-- container -->
<div id="container">
	<!-- side -->
	<div class="side">
		<h3 class="title" title="그룹 관리"><i class="fa fa-folder"></i></h3>
		<!-- group_menu -->
		<div class="btn-group group_menu">
			<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target=".create_group">
				<i class="fa fa-plus"></i>
			</a>
			<a href="#" class="btn btn-default btn-xs del_group_btn" data-toggle="modal" data-target=".delete_group">
				<i class="fa fa-minus"></i>
			</a>
		</div>
		<!-- //group_menu -->
		<!-- group_area -->
		<div class="group_area">
			<ul class="tree droppable">
				<li class="root">
					<a href="#">
						<span class="side_name">
							<i class="fa fa-building-o"></i> <? echo $root->name; ?>
						</span>
					</a>
					<input type="hidden" class="root_index" value="<? echo $root->index; ?>" />
				</li>
<?
	foreach($belong as $group){
?>
				<li class="belong">
					<ul class="belong_tree droppable">
						<li class="belong_root">
							<a href="#">
								<span class="side_name">
									<i class="
<?
		if(get_cookie($group->index)=='close'){
			echo 'fa fa-folder-o';
		}else{
			echo 'fa fa-folder-open-o';
		}
?>
									"></i> <? echo $group->name; ?>
								</span>
							</a>
							<input type="hidden" class="group_name" name="group_name" value="<? echo $group->name; ?>" />
							<input type="hidden" class="isFold" name="isFold" value="
<?
		if(get_cookie($group->index)=='close'){
			echo "'true'";
		}else{
			echo "'false'";
		}
?>
							" />
							<input type="hidden" class="group_index" value="<? echo $group->index; ?>" />
						</li>
<?
		$group_connect_data = array('g_index' => $group->index);
		$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);	//get connect

		foreach($group_connected['pc_connect'] as $belong_pc_connect){
			$belong_pc_data = array('servernum' => $belong_pc_connect->p_index);
			$belong_pc = $this->neturo_server_info_model->getPcOnce($belong_pc_data);	//get belong pc
?>
						<li class="belong_pc draggable" style="
			<?
				if(get_cookie($group->index) == 'close'){
					echo 'display:none;';
				}
			?>
						">
							<a href="#">
								<span class="side_name">
									<i class="fa fa-desktop"></i> <? echo $belong_pc->com_name; ?>
								</span>
							</a>
							<input type="hidden" class="belong_pc_index" value="<? echo $belong_pc->servernum; ?>" />
							<input type="hidden" class="parent_group_index" value="<? echo $group->index; ?>">
						</li>
<?
		} //end of foreach($group_connected['pc_connect'] as $belong_pc_connect)
?>
					</ul>
				</li>
<?
	} //end of foreach($belong as $group)

	foreach($root_pc as $pc){
?>
				<li class="pc draggable">
					<a href="#">
						<span class="side_name">
							<i class="fa fa-desktop"></i> <? echo $pc->com_name; ?>
						</span>
					</a>
					<input type="hidden" class="pc_index" value="<? echo $pc->servernum; ?>" />
					<input type="hidden" class="parent_group_index" value="<? echo $root->index; ?>">
				</li>
<?
	} //end of foreach($root_pc as $pc)
?>
			</ul>
		</div>
		<!-- //group_area -->
	</div>
	<!-- //side -->


<!-- right click menu -->
<ul class="dropdown-menu side_drop_menu" role="menu" aria-labelledby="dLabel" id="root_menu" style="display:none;position:relative; z-index:100;">
	<li>
		<a class="group_add" href="#">
			<i class="fa fa-plus"></i> 그룹 추가
		</a>
	</li>
	<li>
		<a class="group_del" href="#">
			<i class="fa fa-minus"></i> 그룹 삭제
		</a>
	</li>
</ul>
<!-- //right click menu -->

<!-- modal window -->
<?
/**
 * 그룹 이동 팝업
 */
?>
<div class="modal fade move_group" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form class="form-horizontal" method="post" id="move_form" role="form" action="<?echo site_url('monitoring/move_group')?>">
			<!-- modal header -->
			<div class="modal-header header_warning">
				그룹 이동
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<input type="hidden" class="bef_group" name="bef_group"/>
				<input type="hidden" class="sel_pc" name="sel_pc"/>
				<input type="hidden" class="aft_group" name="aft_group"/>
				<div class="alert_header">
					<span class="label_warning">
						<i class="fa fa-desktop"></i><span class="sel_pc_name"></span> 
					</span>
				</div>
				<div class="alert_content">
					<p class="alert_msg">
						<span class="text-warning">
							<i class="fa fa-folder"></i> <span class="bef_group_name"></span>
						</span>에서
						<span class="text-warning">
							<i class="fa fa-folder"></i> <span class="aft_group_name"></span>
						</span>으로
					</p>
					<p class="alert_msg">
						<span class="text-warning">이동</span> 하시겠습니까?
					</p>
				</div>
			</div>
			<!-- modal footer -->
			<div class="modal-footer">
				<button type="submit" class="btn btn-warning btn-sm confirm_btn" title="그룹 이동">
					이동
				</button>
				<button type="button" class="btn btn-default btn-sm cancel_btn"  data-dismiss="modal" title="취소">
					취소
				</button>
			</div>
			</form>
		</div>
	</div>
</div>

<?
/**
 * 그룹 생성 팝업
 */
?>
<div class="modal fade create_group" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form class="form-horizontal" method="post" id="create_form" role="form" action="<?echo site_url('monitoring/create_group')?>">
			<!-- modal header -->
			<div class="modal-header header_primary">
				그룹 생성
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<!-- modal body -->
			<div class="modal-body">
				<div class="alert_header">
					<span class="label_primary">
						<i class="fa fa-folder"></i> NEW
					</span>
				</div>
				<div class="alert_content">
					<label class="create_group_name create_label" for="create_group_name">
						이름
					</label>
					<input type="text" class="form-control input-sm create_input" placeholder="그룹 이름" name="create_group_name" id="create_group_name" />
				</div>
			</div>
			<!-- modal footer -->
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-sm confirm_btn" title="그룹 생성">
					생성
				</button>
				<button type="button" class="btn btn-default btn-sm cancel_btn"  data-dismiss="modal" title="취소">
					취소
				</button>
			</div>
			</form>
		</div>
	</div>
</div>

<?
/**
 * 그룹 삭제 팝업
 */
?>
<div class="modal fade delete_group" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form class="form-horizontal" method="post" id="delete_form" role="form" action="<?echo site_url('monitoring/delete_group')?>">
			<!-- modal header -->
			<div class="modal-header header_danger">
				그룹 삭제
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<!-- modal body -->
			<div class="modal-body">
				<input type="hidden" id="delete_group_name" name="delete_group_name" value="" />
				<div class="alert_header">
					<span class="label_danger">
						 <i class="fa fa-folder"></i> <span class="delete_group_name"></span>
					</span>
				</div>
				<div class="alert_content">
					<p class="alert_msg">
						<span class="text-danger">삭제</span> 하시겠습니까?
					</p>
				</div>
			</div>
			<!-- modal footer -->
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger btn-sm confirm_btn" title="그룹 삭제">
					삭제
				</button>
				<button type="button" class="btn btn-default btn-sm cancel_btn"  data-dismiss="modal" title="취소">
					취소
				</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- //modal window -->