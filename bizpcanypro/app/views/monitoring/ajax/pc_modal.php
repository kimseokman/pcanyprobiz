<?
/**
 * html / wrap / container / content / ajax_pc_modal
 */
?>
<div class="modal fade modal_pc" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- modal header -->
			<div class="modal-header header_success">
				모니터링 - 800 600
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
			</div>
			<!-- modal body -->
			<div class="modal-body">
				<!-- modal footer -->
				<img src="<? echo base_url(); ?>/static/img/monitoring/<? echo $corp_id; ?>/<? echo $pc_index; ?>.png?data=" class="refleshImg modalImg" alt="computer" width="800" height="600"/>
				<div class="alert_content">
					<p class="alert_msg">
						<span class="label_success">
							<i class="fa fa-file-image-o"></i> 이미지
						</span>
						는 5초마다 갱신됩니다.
					</p>
				</div>
			</div>
			<!-- modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-sm confirm_btn" data-dismiss="modal" title="확인">
					확인
				</button>
			</div>
		</div>
	</div>
</div>