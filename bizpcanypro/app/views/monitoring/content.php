<?
/**
 * html / wrap / container / content
 */
?>
	<!-- content -->
	<div id="content">
		<!-- loading -->
		<div class="preloader">
			<img src="<? echo base_url(); ?>/static/img/preloader.gif" class="getdata" alt="preloader"/>
		</div>
		<!-- ajax-content -->
		<div id="ajax_content">
		</div>
	</div>
	<!-- //content -->
</div>
<!-- //container -->