<!-- container -->
<div id="container">
	<!-- login_box -->
	<div class="login_box">
		<!-- login_form -->
		<div class="main_visual">
			<h1 class="text-center">
				<img class="lg_logo" src="<? echo base_url(); ?>/static/img/lg_logo_w.png" alt="PCAnyPro Enterprise Logo"/>
			</h1>
			<p class="text-center">
				<img class="lg_txt_logo" src="<? echo base_url(); ?>/static/img/lg_txt_logo_w.png" alt="PCAnyPro Enterprise" />
			</p>
		</div>
		<div class="login_form">
			<form class="form-horizontal" method="post" role="form" action="<?echo site_url('index/login')?>">
				<fieldset>
					<legend class="sr-only">로그인</legend>
					<p class="id_area">
						<label class="userID sr-only" for="userID">ID </label>
						<input type="text" class="form-control" placeholder="ID" name="userID"/>
					</p>
					<p class="pw_area">
						<label class="userPW sr-only" for="userPW">Password </label>
						<input type="password" class="form-control" placeholder="PASSWORD" name="userPW"/>
					</p>
					<button type="submit" class="login_btn btn btn-primary" title="로그인 버튼">
						SIGN IN
					</button>
				</fieldset>
			</form>
			<p class="forgot">
				<a href="#">
					Forgot your ID or Password?
				</a>
			</p>
		</div>
		<!-- //login_form -->
	</div>
	<!-- //login_box -->
	<ul class="theme">
		<li class="theme1">
			<a href="#">
				<i class="fa fa-desktop"></i>
			</a>
		</li>
		<li class="theme2">
			<a href="#">
				<i class="fa fa-desktop"></i>
			</a>
		</li>
		<li class="theme3">
			<a href="#">
				<i class="fa fa-desktop"></i>
			</a>
		</li>
	</ul>
</div>
<!-- //container -->