<!-- footer -->
<div id="footer">
	<ul class="os_list">
		<li>
			<a href="#">
				<i class="fa fa-android"></i>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-apple"></i>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-windows"></i>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-linux"></i>
			</a>
		</li>
	</ul>
	<p class="copy">Copyright &copy; 2014 KOINO. All Rights Reserved.</p>
	<ul class="fallow_list">
		<li>
			<a href="#">
				<i class="fa fa-facebook"></i>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-twitter"></i>
			</a>
		</li>
		<li>
			<a href="#">
				<i class="fa fa-youtube"></i>
			</a>
		</li>
	</ul>
</div>
<!-- //footer -->