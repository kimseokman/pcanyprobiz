<?
/**
 * html / wrap / container / side
 */
?>
<!-- container -->
<div id="container">
	<!-- side -->
	<div class="side">
		<h3 class="title" title="통계"><i class="fa fa-bar-chart-o"></i></h3>
		<!-- stats_nav -->
		<div class="stats_nav_area">
			<ul class="stats_nav">
				<li>
					<a href="#">
						사용자별 통계
					</a>
				</li>
				<li>
					<a href="#">
						서버별 통계
					</a>
				</li>
				<li>
					<a href="#">
						일자별 통계
					</a>
				</li>
				<li>
					<a href="#">
						서버 유형별 통계
					</a>
				</li>
			</ul>
		</div>
		<!-- //stats_nav -->
	</div>
	<!-- //side -->