<?
/**
 * html / wrap / container / content / ajax_content
 */
?>
		<div class="content_menu_panel">
			<form class="form-horizontal" method="post" role="form" action="<?echo site_url('user_manage/reload')?>">
				<div class="content_menu">
				<button type="submit" class="btn btn-primary btn-xs refresh_btn">
					<i class="fa fa-refresh fa-spin"></i>
				</button>
				</div>
			</form>
		</div>
		<ul class="list_view">
<?
$group_data = array('u_index' => $user_index);
$user_group = $this->pcany_group_model->getGroup($group_data);

foreach($user_group as $group){
	$user_connect_data = array('g_index' => $group->index);
	$temp['user_group_connect'] = $this->pcany_group_pc_connect_model->getConnect($user_connect_data);

	foreach($temp['user_group_connect'] as $user_pc_connect){
		$user_pc_data = array('servernum' => $user_pc_connect->p_index);		//pc data setting
		$user_pc = $this->neturo_server_info_model->getPcOnce($user_pc_data);				//pc data get
?>
			<li class="lg_pc draggable">
				<a href="#">
					<i class="fa fa-desktop"></i>
					<span class="text-center veiw_name"><? echo $user_pc->com_name; ?></span>
				</a>
				<input type="hidden" class="pc_index" value="<? echo $user_pc->servernum; ?>" />
			</li>
<?
	}//end of foreach($temp['user_group_connect'] as $user_pc_connect)
}//end of foreach($user_group as $group)
?>
		</ul>

<?
	/**
	 * ajax 후 jquery 이벤트 적용을 위한 파일 - build 순서에 상관이 있음.
	 * 따라서 side.js와 중복되는 이벤트는 되도록 피할 것.
	 */
?>
<script src="<? echo base_url(); ?>/static/js/user_manage_content.js"></script>