<?
/**
 * html / wrap / container / side
 */
?>
<!-- container -->
<div id="container">
	<!-- side -->
	<div class="side">
		<h3 class="title" title="환경설정"><i class="fa fa-cog"></i></h3>
		<!-- conf_nav -->
		<div class="conf_nav_area">
			<ul class="conf_nav">
				<li>
					<a href="#">
						개인 설정
					</a>
				</li>
				<li>
					<a href="#">
						화면 설정
					</a>
				</li>
				<li>
					<a href="#">
						원격제어 화면 보안 설정
					</a>
				</li>
				<li>
					<a href="#">
						뷰어 기능 설정
					</a>
				</li>
				<li class="has_sub">
					<a href="#">
						계정 설정
					</a>
					<ul class="sub_menu">
						<li>
							<a href="#">
								사용자 정보
							</a>
						</li>
						<li>
							<a href="#">
								회사 정보
							</a>
						</li>
						<li>
							<a href="#">
								연락처 정보
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#">
						기본 설정
					</a>
				</li>
				<li>
					<a href="#">
						허용 IP, MAC 설정
					</a>
				</li>
				<li>
					<a href="#">
						비밀번호 보안 설정
					</a>
				</li>
				<li>
					<a href="#">
						e-mail 설정
					</a>
				</li>
				<li>
					<a href="#">
						라이센스 정보
					</a>
				</li>
			</ul>
		</div>
		<!-- //conf_nav -->
	</div>
	<!-- //side -->