<?
/**
 * html / wrap / container / content / ajax_content
 */
?>
		<div class="content_menu_panel">
			<form class="form-horizontal" method="post" role="form" action="<?echo site_url('pc_manage/reload')?>">
				<div class="content_menu">
				<button type="submit" class="btn btn-primary btn-xs refresh_btn">
					<i class="fa fa-refresh fa-spin"></i>
				</button>
				</div>
			</form>
		</div>
		<ul class="list_view">
<?
if($belong != null){
	foreach($belong as $group){
?>	
			<li class="lg_group lg_droppable">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span class="text-center veiw_name"><? echo $group->name; ?></span>
				</a>
				<input type="hidden" class="group_index" value="<? echo $group->index; ?>" />
			</li>
<?
	}//end of foreach($belong as $group)
}//end of if($belong != null)

foreach($pc as $belong_pc){
?>
			<li class="lg_pc lg_draggable">
				<a href="#">
					<i class="fa fa-desktop"></i>
					<span class="text-center veiw_name"><? echo $belong_pc->com_name; ?></span>
				</a>
				<input type="hidden" class="pc_index" value="<? echo $belong_pc->servernum; ?>" />
				<input type="hidden" class="parent_group_name" value="<? echo $current_group->name; ?>">
				<input type="hidden" class="parent_group_index" value="<? echo $current_group->index; ?>">
			</li>
<?
}
?>
		</ul>

<?
	/**
	 * ajax 후 jquery 이벤트 적용을 위한 파일 - build 순서에 상관이 있음.
	 * 따라서 side.js와 중복되는 이벤트는 되도록 피할 것.
	 */
?>
<script src="<? echo base_url(); ?>/static/js/pc_manage_content.js"></script>