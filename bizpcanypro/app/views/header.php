<?
/**
 * html / wrap / Header
 */
?>
<!-- header -->
<div id="header">
	<h1 class="logo">
		<a href="<? echo site_url('index')?>">
			<img src="<? echo base_url(); ?>/static/img/txt_logo_w.png" alt="pcanypro" />
		</a>
	</h1>
	<!-- top_panel -->
	<div class="top_panel">
		<!-- top_menu_panel -->
		<div class="top_menu_panel">
			<ul class="top_menu">
<?
if($isLogin){
?>
				<li>
					<a href="#" class="show_side">
						<i class="fa fa-bars" title="그룹 관리"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="#" id="user_menu" data-toggle="dropdown">
						<i class="fa fa-user" title="사용자 정보"></i>
					</a>
					<ul class="dropdown-menu top_drop_menu">
						<li>
							<a class="logout" href="#">로그아웃</a>
						</li>
					</ul>
				</li>
<?
}//end of if($isLogin)
?>
				<li class="dropdown">
					<a href="#" id="trans_menu" data-toggle="dropdown">
						<i class="fa fa-globe" title="번역"></i>
					</a>
					<ul class="dropdown-menu top_drop_menu">
						<li><a href="#">한글</a></li>
						<li><a href="#">English</a></li>
						<li><a href="#">中國語</a></li>
						<li><a href="#">わご</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- //top_menu_panel -->
		<!-- gnb_panel -->
		<div class="gnb_panel">
			<ul class="gnb">
<?
if($isLogin){
?>
				<li>
					<a href="<? echo site_url('pc_manage')?>" class="<? echo $pc_manage; ?>">
						PC 관리
					</a>
				</li>
				<li>
					<a href="<? echo site_url('monitoring') ?>" class="<? echo $monitoring; ?>">
						모니터링
					</a>
				</li>
				<li>
					<a href="<? echo site_url('stats')?>" class="<? echo $stats; ?>">
						통계
					</a>
				</li>
				<li>
					<a href="<? echo site_url('conf')?>" class="<? echo $conf; ?>">
						환경설정
					</a>
				</li>
<?
	if($isAdmin){
?>
				<li>
					<a href="<? echo site_url('user_manage')?>" class="<? echo $user_manage; ?>">
						사용자 관리
					</a>
				</li>
<?
	}//end of if($isAdmin)
?>
<?
}//end of if($isLogin)
?>
			</ul>
		</div>
		<!-- //gnb_panel -->
	</div>
	<!-- //top_panel -->
</div>
<!-- //header -->