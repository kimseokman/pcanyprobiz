<?
/**
 * database : pcanypro
 * table : pcany_group
 * - 모든 계정에 대한 그룹 정보를 담고 있는 테이블
 **
 * desc
 **
 * isGroup()		- 해당 group 유무
 * getGroupOnce()	- group data 단일 출력
 * getGroup()		- group data 복수 출력
 * setGroup()		- group data 입력
 * updateGroup()	- group data 수정
 * delGroup()		- group data 삭제
 *
 * 2014-07-30
 * by KSM
 */
class Pcany_group_model extends CI_Model{
	function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function isGroup($data){
		$is_group = $this->db->get_where('pcany_group', $data)->row();
		if($is_group){
			return true;
		}else{
			return false;
		}
	}

	public function getGroup($data){
		return $this->db->get_where('pcany_group', $data)->result();
	}

	public function getGroupOnce($data){
		return $this->db->get_where('pcany_group', $data)->row();
	}

	public function setGroup($data){
		$this->db->insert('pcany_group', $data);
	}

	public function updateGroup($group, $data){
		$this->db->where('index', $group->index);
		$this->db->update('pcany_group', $data);
	}

	public function delGroup($data){
		$this->db->delete('pcany_group', $data);
	}
}
?>