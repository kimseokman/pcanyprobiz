<?
/**
 * database : pcanypro
 * table : neturo_server_info
 * - server(PC) 에한 정보를 담고 있는 테이블
 **
 * desc
 **
 * isPc()		- 해당 pc 유무
 * getPcOnce()	- pc data 단일 출력
 * getPc()		- pc data 복수 출력
 * updatePc()	- pc data 수정
 *
 * 2014-07-30
 * by KSM
 */
class Neturo_server_info_model extends CI_Model{
	function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function isPc($data){
		$is_pc = $this->db->get_where('neturo_server_info', $data)->row();
		if($is_pc){
			return true;
		}else{
			return false;
		}
	}

	public function getPcOnce($data){
		return $this->db->get_where('neturo_server_info', $data)->row();
	}

	public function getPc($data){
		return $this->db->get_where('neturo_server_info', $data)->result();
	}

	public function updatePc($pc, $data){
		$this->db->where('servernum', $pc->servernum);
		$this->db->update('neturo_server_info', $data);
	}
}
?>