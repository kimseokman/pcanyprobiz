<?
/**
 * database : pcanypro
 * table : neturo_user
 * - 사용자에 대한 회사 정보를 담고 있는 테이블
 **
 * desc
 **
 * setCorp()	- 회사 정보 입력
 * getCorpOnce()- 회사 정보 단일 출력
 * getCorp()	- 회사 정보 복수 출력
 *
 * 2014-07-30
 * by KSM
 */
class Neturo_user_model extends CI_Model{
	function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function setCorp($data){
		$this->db->insert('neturo_user', $data);
	}

	public function getCorpOnce($data){
		return $this->db->get_where('neturo_user', $data)->row();
	}

	public function getCorp($data){
		return $this->db->get_where('neturo_user', $data)->result();
	}
}
?>