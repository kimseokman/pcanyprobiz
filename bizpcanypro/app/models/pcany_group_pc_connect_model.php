<?
/**
 * database : pcanypro
 * table : pcany_group_pc_connect
 * - 그룹 정보를 담고 있는 테이블과
 *   server(pc) 정보를 담고 있는 테이블이 서로 n:n 관계를 성립하면서
 *   출력시 중복을 피하기 위해 만들어진 connect
 * - group 에 대한 pc 또는 pc가 속해 있는 group 정보를 출력 시에
 *	 항상 connect 테이블을 이용한다
 * - 해당 연결 고리가 수정시 중복을 허용할 수 있기 때문에
 *   수정하지 않고 원래 connect를 삭제 후 새 connect를 입력하는 형식
 **
 * desc
 **
 * getConnectOnce()	- connect 단일 출력
 * getConnect()		- connect 복수 출력
 * setConnect()		- connect 입력
 * delConnect()		- connect 삭제
 *
 * 2014-07-30
 * by KSM
 */
class Pcany_group_pc_connect_model extends CI_Model{
	function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function getConnectOnce($data){
		return $this->db->get_where('pcany_group_pc_connect', $data)->row();
	}

	public function getConnect($data){
		return $this->db->get_where('pcany_group_pc_connect', $data)->result();
	}

	public function setConnect($data){
		$this->db->insert('pcany_group_pc_connect', $data);
	}

	public function delConnect($data){
		$this->db->delete('pcany_group_pc_connect', $data);
	}
}
?>