<?
/**
 * database : pcanypro
 * table : pcany_member
 * - 모든 계정 정보를 담고 있는 테이블
 * - member 테이블을 기준으로 -> group 테이블 -> connect 테이블 -> server_info 테이블로 이어지고
 *   member 테이블을 기준으로 corp 테이블과 n:1 역관계를 형성
 *   따라서, 새로운 계정이 입력되면 해당 계정은 자신의 회사정보가 있는 테이블의 row index를 가지고 있으며 
 *   이는 중복을 허용하고(한 회사에 여러 계정) 이후에 corp 정보를 입력하는 형식과 유사한 로직으로 구성해야한다.
 **
 * desc
 **
 * setUser()		- 사용자 입력
 * delUser()		- 사용자 삭제
 * getUser()		- 사용자 복수 출력
 * getUserOnce()	- 사용자 단일 출력
 *
 * 2014-07-30
 * by KSM
 */
class Pcany_member_model extends CI_Model{
	function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->database();
	}

	public function setUser($data){
		$this->db->insert('pcany_member', $data);
	}

	public function delUser($data){
		$this->db->delete('pcany_member', $data);
	}

	public function getUser($data){
		return $this->db->get_where('pcany_member', $data)->result();
	}

	public function getUserOnce($data){
		return $this->db->get_where('pcany_member', $data)->row();
	}
}
?>