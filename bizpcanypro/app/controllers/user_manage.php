<? if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * 사용자 관리 페이지 controller
 **
 * desc
 **
 * index()						- 기본 뷰
 * view_content()				- 비어 있는 content 보여주기
 * ajax_view_content()			- 빈 content 위에 새로운 content 비동기화 뷰
 * veiw_side()					- side bar 뷰
 * view_gnb()					- 주메뉴 뷰
 * reload()						- 새로 고침 기능
 * create_user()				- 사용자 생성 기능
 * delete_user()				- 사용자 삭제 기능
 * copy_pc()					- pc 권한 할당
 * remove_pc()					- pc 권한 해제
 *
 * 2014-07-30
 * by KSM
 */

class User_manage extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('pcany_member_model');
		$this->load->model('pcany_group_model');
		$this->load->model('pcany_group_pc_connect_model');
		$this->load->model('neturo_server_info_model');
		$this->load->model('neturo_user_model');
	}

	public function index(){
		if($this->session->userdata('isLogin')){
			//$this->output->enable_profiler(TRUE);
			$file_name = "user_manage";
			$title = "사용자 관리 페이지";
			$this->load->view('html_head', array('file_name' => $file_name, 'title' => $title));
			$this->view_gnb();
			$this->view_side();
			$this->view_content();
			$this->load->view('html_foot', array('file_name' => $file_name));
		}else{
			redirect('index');
		}
	}

	/**
	 * content -> ajax_content 영역
	 */
	public function ajax_view_content(){
		$user_index = TRIM($this->input->post('user_index', true));

		$data['user_index'] = $user_index;

		$this->load->view('user_manage/ajax/content', $data);
	}

	/**
	 * content 영역
	 */
	public function view_content(){
		$this->load->view('user_manage/content');
	}

	/**
	 * side 영역
	 */
	public function view_side(){
		$admin_data = array('id'=>$this->session->userdata('id'));															
		$data['admin'] = $this->pcany_member_model->getUserOnce($admin_data);					//get admin

		$belong_all_user_data = array('c_index' => $data['admin']->c_index, 'isAdmin' => 0);
		$data['belong_user'] = $this->pcany_member_model->getUser($belong_all_user_data);		//get etc users

		$this->load->view('user_manage/side', $data);
	}

	/**
	 * 주메뉴
	 */
	public function view_gnb(){
		$pc_manage_menu = "";
		$monitoring_menu = "";
		$stats_menu = "";
		$conf_menu = "";
		$user_manage_menu = "active";

		$header_param = array('isAdmin' => $this->session->userdata('isAdmin'), 
							'pc_manage' => $pc_manage_menu,
							'monitoring' => $monitoring_menu,
							'stats' => $stats_menu,
							'conf' => $conf_menu,
							'user_manage' => $user_manage_menu,
							'isLogin' => $this->session->userdata('isLogin')
							);

		$this->load->view('header', $header_param);
	}

	/**
	 * 새로 고침
	 */
	public function reload(){
		redirect('user_manage');
	}
	
	/**
	 * 사용자 생성
	 */
	public function create_user(){
		$user_id = TRIM($this->input->post('user_id', true));
		$user_pw = MD5(TRIM($this->input->post('user_pw', true)));
		$user_name = TRIM($this->input->post('user_name', true));

		$admin_data = array('id'=>$this->session->userdata('id'));
		$admin = $this->pcany_member_model->getUserOnce($admin_data);				//get admin

		$set_user_data = array('c_index' => $admin->c_index, 'id' => $user_id, 'pw' => $user_pw, 'name' => $user_name, 'isAdmin'=>0);	
		$this->pcany_member_model->setUser($set_user_data);							//set belong user

		$get_user_data = array('c_index' => $admin->c_index, 'id' => $user_id);
		$user = $this->pcany_member_model->getUserOnce($get_user_data);				//get belong user

		$corp_data = array('usernum' => $user->c_index);
		$corp = $this->neturo_user_model->getCorpOnce($corp_data);					//get corp of user -> for naming group of belong user

		$set_group_data = array('u_index' => $user->index, 'name' => $corp->name, 'isRoot' => 1);
		$this->pcany_group_model->setGroup($set_group_data);						//set root group of belong user
		
		redirect('user_manage');
	}
	
	/**
	 * 사용자 삭제
	 */
	public function delete_user(){
		$user_id = TRIM($this->input->post('select_user', true));

		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);										//get user

		$del_user_data = array('c_index' => $user->c_index, 'id' => $user_id);							//for duplication id of user
		$del_user = $this->pcany_member_model->getUserOnce($del_user_data);								//retry get user

		$del_group_data = array('u_index' => $del_user->index);									
		$del['group'] =  $this->pcany_group_model->getGroup($del_group_data);							//del group data get

		foreach($del['group'] as $del_group){
			$del_group_connect_data = array('g_index' => $del_group->index);
			$this->pcany_group_pc_connect_model->delConnect($del_group_connect_data);					//del connect
		}

		$this->pcany_group_model->delGroup($del_group_data);	//del group
		$this->pcany_member_model->delUser($del_user_data);		//del user

		redirect('user_manage');
	}
	
	/**
	 * PC 권한 할당
	 */
	public function copy_pc(){
		$aft_user_id = TRIM($this->input->post('aft_user', true));				//selected user
		$sel_pc_name =  TRIM($this->input->post('sel_pc', true));				//selected PC
		
		//확실히 해당 유저를 찾기 위해 db 쿼리
		$admin_data = array('id'=>$this->session->userdata('id'));
		$admin = $this->pcany_member_model->getUserOnce($admin_data);			//get admin

		$corp_data = array('usernum' => $admin->c_index);
		$corp = $this->neturo_user_model->getCorpOnce($corp_data);				//get corp

		$copy_user_data = array('c_index' => $admin->c_index, 'id' => $aft_user_id);
		$copy_user = $this->pcany_member_model->getUserOnce($copy_user_data);	//get user

		$pc_data = array('com_name' => $sel_pc_name);		
		$sel_pc = $this->neturo_server_info_model->getPcOnce($pc_data);			//get pc
		
		$aft_group_data = array('u_index' => $copy_user->index, 'isRoot' => 1);					
		$aft_group = $this->pcany_group_model->getGroupOnce($aft_group_data);	//get root group of etc user

		$aft_group_pc_connect_data = array('g_index' => $aft_group->index, 'p_index' => $sel_pc->servernum);	
		$this->pcany_group_pc_connect_model->setConnect($aft_group_pc_connect_data);	//set connect

		redirect('user_manage');
	}

	/**
	 * PC 권한 해제
	 */
	public function remove_pc(){
		$user_index = TRIM($this->input->post('belong_to_user', true));		
		$pc_index =  TRIM($this->input->post('select_pc_index', true));	

		$user_data = array('index'=>$user_index);
		$user = $this->pcany_member_model->getUserOnce($user_data);			//get user

		$group_data = array('u_index' => $user->index);					
		$user_group = $this->pcany_group_model->getGroup($group_data);		//get group

		$pc_data = array('servernum' => $pc_index);		
		$pc = $this->neturo_server_info_model->getPcOnce($pc_data);			//get pc

		foreach($user_group as $group){
			$group_connect_data = array('g_index'=>$group->index, 'p_index'=>$pc->servernum);
			$this->pcany_group_pc_connect_model->delConnect($group_connect_data);		//del connect
		}

		redirect('user_manage');
	}
}
?>