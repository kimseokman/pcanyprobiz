<? if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * PC 관리 페이지 controller
 **
 * desc
 **
 * index()				- 기본 뷰
 * view_content()		- 비어 있는 content 보여주기
 * ajax_view_content()	- 빈 content 위에 새로운 content 비동기화 뷰
 * veiw_side()			- side bar 뷰
 * view_gnb()			- 주메뉴 뷰
 * reload()				- 새로 고침 기능
 * create_group()		- 그룹 생성 기능
 * delete_group()		- 그룹 삭제 기능
 * move_group()			- 그룹 이동 기능
 *
 * 2014-07-23
 * by KSM
 */

class Pc_manage extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('pcany_member_model');
		$this->load->model('pcany_group_model');
		$this->load->model('pcany_group_pc_connect_model');
		$this->load->model('neturo_server_info_model');
	}

	public function index(){	//default PC manage
		if($this->session->userdata('isLogin')){
			//$this->output->enable_profiler(TRUE);
			$file_name = "pc_manage";
			$title = "PC 관리 페이지";
			$this->load->view('html_head', array('file_name' => $file_name, 'title' => $title));
			$this->view_gnb();
			$this->view_side();
			$this->view_content();
			$this->load->view('html_foot', array('file_name' => $file_name));
		}else{
			redirect('index');
		}
	}

	/**
	 * content -> ajax_content 영역
	 */
	public function ajax_view_content(){
		$case_by = TRIM($this->input->post('case_by', true));
		$pc_index = TRIM($this->input->post('pc_index', true));
		$group_index = TRIM($this->input->post('group_index', true));
		
		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);				//get user
		
		$data['current_pc'] = null;
		$data['current_group'] = null;
		/**
		 * no ajax -> ajax init -> case_by : root
		 */
		if($case_by == null || $case_by == '')	$case_by = 'root';				//init case_by
		
		/**
		 * ajax - root click
		 */
		if($case_by == 'root'){
			$root_data = array('u_index' => $user->index, 'isRoot' => 1);		
			$root = $this->pcany_group_model->getGroupOnce($root_data);			//root data get

			$group_index = $root->index;										//init group_index
			$pc_index = '';														//init pc_index

			$data['current_group'] = $root;

			$root_connect_data = array('g_index' => $group_index);													
			$root_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($root_connect_data);	//get connect of root

			$root_pc_list = array();
			foreach($root_connected['pc_connect'] as $root_pc_connect){
				$root_pc_data = array('servernum' => $root_pc_connect->p_index);					
				$root_pc = $this->neturo_server_info_model->getPcOnce($root_pc_data);								//get pc of root
				array_push($root_pc_list, $root_pc);
			}
			$data['pc'] = $root_pc_list;
			
			$group_data = array('u_index' => $user->index, 'isRoot' => 0);
			$data['belong'] = $this->pcany_group_model->getGroup($group_data);										//get group
		}
		
		/**
		 * ajax - group click
		 */
		if($case_by == 'group'){
			$group_data = array('index' => $group_index);
			$group = $this->pcany_group_model->getGroupOnce($group_data);											//get group

			$data['current_group'] = $group;

			$group_connect_data = array('g_index' => $group_index);														
			$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);	//get connect of group

			$group_pc_list = array();
			foreach($group_connected['pc_connect'] as $group_pc_connect){
				$group_pc_data = array('servernum' => $group_pc_connect->p_index);
				$group_pc = $this->neturo_server_info_model->getPcOnce($group_pc_data);								//get pc
				array_push($group_pc_list, $group_pc);
			}
			$data['pc'] = $group_pc_list;

			$data['belong'] = null;																					//set belong group
		}

		/**
		 * ajax - pc click
		 */
		if($case_by == 'pc'){
			$group_data = array('index' => $group_index);
			$group = $this->pcany_group_model->getGroupOnce($group_data);											//get group

			$data['current_group'] = $group;

			$pc_data = array('servernum' => $pc_index);
			$pc = $this->neturo_server_info_model->getPcOnce($pc_data);												//get pc
			$data['current_pc'] = $pc;

			$group_connect_data = array('g_index' => $group_index);														
			$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);	//get connect of group

			$group_pc_list = array();
			foreach($group_connected['pc_connect'] as $group_pc_connect){
				$group_pc_data = array('servernum' => $group_pc_connect->p_index);
				$group_pc = $this->neturo_server_info_model->getPcOnce($group_pc_data);								//get pc
				array_push($group_pc_list, $group_pc);
			}
			$data['pc'] = $group_pc_list;

			$group_data = array('u_index' => $user->index, 'isRoot' => 0);
			$data['belong'] = $this->pcany_group_model->getGroup($group_data);										//get group
		}

		/**
		 * ajax - belong pc click
		 */
		if($case_by == 'belong_pc'){
			$group_data = array('index' => $group_index);
			$group = $this->pcany_group_model->getGroupOnce($group_data);											//get group

			$data['current_group'] = $group;

			$pc_data = array('servernum' => $pc_index);
			$pc = $this->neturo_server_info_model->getPcOnce($pc_data);												//get pc
			$data['current_pc'] = $pc;

			$group_connect_data = array('g_index' => $group_index);														
			$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);	//get connect of group

			$group_pc_list = array();
			foreach($group_connected['pc_connect'] as $group_pc_connect){
				$group_pc_data = array('servernum' => $group_pc_connect->p_index);
				$group_pc = $this->neturo_server_info_model->getPcOnce($group_pc_data);								//get pc
				array_push($group_pc_list, $group_pc);
			}
			$data['pc'] = $group_pc_list;

			$data['belong'] = null;																					//get group
		}

		$this->load->view('pc_manage/ajax/content', $data);
	}

	/**
	 * content 영역
	 */
	public function view_content(){
		$this->load->view('pc_manage/content');
	}
	
	/**
	 * side 영역
	 */
	public function view_side(){
		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);			//get current user

		$root_data = array('u_index' => $user->index, 'isRoot' => 1);
		$data['root'] = $this->pcany_group_model->getGroupOnce($root_data);	//get root of user

		$root_connect_data = array('g_index' => $data['root']->index);
		$root_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($root_connect_data);//get connect
		
		$root_pc_list = array();											
		foreach($root_connected['pc_connect'] as $root_pc_connect){
			$root_pc_data = array('servernum' => $root_pc_connect->p_index);
			$root_pc = $this->neturo_server_info_model->getPcOnce($root_pc_data);
			array_push($root_pc_list, $root_pc);
		}

		$data['root_pc'] = $root_pc_list;									//get pc of root

		$group_data = array('u_index' => $user->index, 'isRoot' => 0);
		$data['belong'] = $this->pcany_group_model->getGroup($group_data);	//get etc group
		
		$this->load->view('pc_manage/side',	$data);							//send & view							
	}
	
	/**
	 * 주메뉴
	 */
	public function view_gnb(){
		$pc_manage_menu = "active";
		$monitoring_menu = "";
		$stats_menu = "";
		$conf_menu = "";
		$user_manage_menu = "";

		$header_param = array('isAdmin' => $this->session->userdata('isAdmin'), 
							'pc_manage' => $pc_manage_menu,
							'monitoring' => $monitoring_menu,
							'stats' => $stats_menu,
							'conf' => $conf_menu,
							'user_manage' => $user_manage_menu,
							'isLogin' => $this->session->userdata('isLogin')
							);

		$this->load->view('header', $header_param);
	}
	
	/**
	 * 새로 고침
	 */
	public function reload(){
		redirect('pc_manage');
	}
	
	/**
	 * 그룹 생성
	 */
	public function create_group(){
		$group_name = TRIM($this->input->post('create_group_name', true));

		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);								//get user

		$group_data = array('u_index' => $user->index, 'name' => $group_name, 'isRoot' => 0);	
		$this->pcany_group_model->setGroup($group_data);										//set group
		
		$group = $this->pcany_group_model->getGroupOnce($group_data);							//get group

		$cookie = array('name' => $group->index, 'value' => "open", 'expire' => 0);				
		$this->input->set_cookie($cookie);														//set cookie

		redirect('pc_manage');
	}
	
	/**
	 * 그룹 삭제
	 */
	public function delete_group(){
		$group_name = TRIM($this->input->post('delete_group_name', true));

		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);									//get user

		$group_data = array('u_index' => $user->index, 'name' => $group_name, 'isRoot' => 0);	
		$root_data = array('u_index' => $user->index, 'isRoot' => 1);							

		$group = $this->pcany_group_model->getGroupOnce($group_data);								//get group
		$root = $this->pcany_group_model->getGroupOnce($root_data);									//get root
		$cookie = array('name' => $group->index);													//cookie

		$this->pcany_group_model->delGroup($group_data);											//del group

		$group_pc_connect = array('g_index' => $group->index);									
		$group_pc['connect'] = $this->pcany_group_pc_connect_model->getConnect($group_pc_connect);	//get connect
		foreach($group_pc['connect'] as $connected){
			$init_connect_data = array('g_index' => $root->index, 'p_index'=>$connected->p_index);	
			$this->pcany_group_pc_connect_model->setConnect($init_connect_data);					//init connect of root
			$del_connect_data = array('index' => $connected->index);								
			$this->pcany_group_pc_connect_model->delConnect($del_connect_data);						//del connect of group
		}

		redirect('pc_manage');
	}
	
	/**
	 * 그룹 이동
	 */
	public function move_group(){
		$bef_group_name = TRIM($this->input->post('bef_group', true));											//before Group
		$aft_group_name = TRIM($this->input->post('aft_group', true));											//after Group
		$sel_pc_name =  TRIM($this->input->post('sel_pc', true));												//selected PC
		
		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);												//get user
		
		$bef_group_data = array('u_index' => $user->index, 'name' => $bef_group_name);			
		$bef_group = $this->pcany_group_model->getGroupOnce($bef_group_data);									//get before group

		$pc_data = array('com_name' => $sel_pc_name);		
		$sel_pc = $this->neturo_server_info_model->getPcOnce($pc_data);											//get pc

		$bef_group_pc_connect_data = array('g_index' => $bef_group->index, 'p_index' => $sel_pc->servernum);	
		$this->pcany_group_pc_connect_model->delConnect($bef_group_pc_connect_data);							//del connect

		$aft_group_data = array('u_index' => $user->index, 'name' => $aft_group_name);						
		$aft_group = $this->pcany_group_model->getGroupOnce($aft_group_data);									//get after group

		$aft_group_pc_connect_data = array('g_index' => $aft_group->index, 'p_index' => $sel_pc->servernum);	
		$this->pcany_group_pc_connect_model->setConnect($aft_group_pc_connect_data);							//set connect

		redirect('pc_manage');
	}
}
?>