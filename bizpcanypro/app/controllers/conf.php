<? if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * 환경설정 controller
 **
 * desc
 **
 * index()						- 기본 뷰
 *
 * 2014-07-31
 * by KSM
 */

class Conf extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('pcany_member_model');
		$this->load->model('pcany_group_model');
		$this->load->model('pcany_group_pc_connect_model');
		$this->load->model('neturo_server_info_model');
		$this->load->model('neturo_user_model');
	}

	public function index(){
		if($this->session->userdata('isLogin')){
			//$this->output->enable_profiler(TRUE);
			$file_name = "conf";
			$title = "환경설정 페이지";
			$this->load->view('html_head', array('file_name' => $file_name, 'title' => $title));
			$this->view_gnb();
			$this->view_side();
			$this->view_content();
			$this->load->view('html_foot', array('file_name' => $file_name));
		}else{
			redirect('index');
		}
	}

	/**
	 * content 영역
	 */
	public function view_content(){
		$this->load->view('conf/content');
	}

	/**
	 * side 영역
	 */
	public function view_side(){
		$this->load->view('conf/side');
	}

	/**
	 * 주메뉴
	 */
	public function view_gnb(){
		$pc_manage_menu = "";
		$monitoring_menu = "";
		$stats_menu = "";
		$conf_menu = "active";
		$user_manage_menu = "";

		$header_param = array('isAdmin' => $this->session->userdata('isAdmin'), 
							'pc_manage' => $pc_manage_menu,
							'monitoring' => $monitoring_menu,
							'stats' => $stats_menu,
							'conf' => $conf_menu,
							'user_manage' => $user_manage_menu,
							'isLogin' => $this->session->userdata('isLogin')
							);

		$this->load->view('header', $header_param);
	}

	/**
	 * 새로 고침
	 */
	public function reload(){
		redirect('conf');
	}
}
?>