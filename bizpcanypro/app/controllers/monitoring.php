<? if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * PC 관리 페이지 controller
 **
 * desc
 **
 * index()						- 기본 뷰
 * ajax_view_content_modal()	- pc modal 뷰
 * view_content()				- 비어 있는 content 보여주기
 * ajax_view_content()			- 빈 content 위에 새로운 content 비동기화 뷰
 * veiw_side()					- side bar 뷰
 * view_gnb()					- 주메뉴 뷰
 * reload()						- 새로 고침 기능
 * create_group()				- 그룹 생성 기능
 * delete_group()				- 그룹 삭제 기능
 * move_group()					- 그룹 이동 기능
 *
 * 2014-07-29
 * by KSM
 */

class Monitoring extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('pcany_member_model');
		$this->load->model('pcany_group_model');
		$this->load->model('pcany_group_pc_connect_model');
		$this->load->model('neturo_server_info_model');
		$this->load->model('neturo_user_model');
	}

	public function index(){
		if($this->session->userdata('isLogin')){
			//$this->output->enable_profiler(TRUE);
			$file_name = "monitoring";
			$title = "모니터링 페이지";
			$this->load->view('html_head', array('file_name' => $file_name, 'title' => $title));
			$this->view_gnb();
			$this->view_side();
			$this->view_content();
			$this->load->view('html_foot', array('file_name' => $file_name));
		}else{
			redirect('index');
		}
	}

	public function ajax_viewContent(){
		$s_index = TRIM($this->input->post('s_index', true));
		$g_index = TRIM($this->input->post('g_index', true));

		$data['current_pc'] = null;
		$data['current_group'] = null;
		$data['corp'] = null;
		$case_by_root = false;

		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);						//user data get

		$corp_data = array('usernum'=>$user->c_index);
		$corp = $this->neturo_user_model->getCorpOnce($corp_data);
		$data['corp'] = $corp;
		//echo $corp->usernum;
		
		if($s_index == null){	//g_index init
			$root_data = array('u_index' => $user->index, 'isRoot' => 1);		//root data setting
			$root = $this->pcany_group_model->getGroupOnce($root_data);				//root data get
			
			$g_index = $root->index;
			$data['current_group'] = $root;
			$case_by_root = true;
		}else{
			$sel_data = array('index' => $s_index);
			$is_group = $this->pcany_group_model->isGroup($sel_data);

			if($is_group){
				$g_index = $s_index;
			}else{
				$sel_pc_data = array('servernum' => $s_index);
				
				$sel_pc = $this->neturo_server_info_model->getPcOnce($sel_pc_data);
				$data['current_pc'] = $sel_pc;
				
				$g_index = TRIM($this->input->post('g_index', true));
				
				$case_by_root = false;
			}
			
			$sel_group_data = array('index' => $g_index);
			$sel_group = $this->pcany_group_model->getGroupOnce($sel_group_data);

			if($sel_group->isRoot){
				$case_by_root = true;
			}

			$data['current_group'] = $sel_group;	
		}
		
		if($case_by_root){
			$root_connect_data = array('g_index' => $g_index);												//root connect setting
			$root_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($root_connect_data);	//root connected pc connect get

			$root_pc_list = array();
			foreach($root_connected['pc_connect'] as $root_pc_connect){
				$root_pc_data = array('servernum' => $root_pc_connect->p_index);		//pc data setting
				$root_pc = $this->neturo_server_info_model->getPcOnce($root_pc_data);				//pc data get
				array_push($root_pc_list, $root_pc);
			}
			$data['pc'] = $root_pc_list;
			
			$group_data = array('u_index' => $user->index, 'isRoot' => 0);
			$data['belong'] = $this->pcany_group_model->getGroup($group_data);			//group data get
		}else{
			$group_connect_data = array('g_index' => $g_index);														//group connect setting
			$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);		//group connected pc connect get
			
			$group_pc_list = array();
			foreach($group_connected['pc_connect'] as $group_pc_connect){
				$group_pc_data = array('servernum' => $group_pc_connect->p_index);		//pc data setting
				$group_pc = $this->neturo_server_info_model->getPcOnce($group_pc_data);				//pc data get
				array_push($group_pc_list, $group_pc);
			}
			$data['pc'] = $group_pc_list;

			$data['belong'] = null;			//group data get
		}
		$this->load->view('monitoring/content', $data);
	}

	/**
	 * content -> ajax_content -> ajax_pc_modal영역
	 */
	public function ajax_view_content_modal(){
		$pc_index = TRIM($this->input->post('pc_index', true));
		$corp_id = TRIM($this->input->post('corp_id', true));

		$pc_modal_data = array('pc_index' => $pc_index, 'corp_id' => $corp_id);
		
		$this->load->view('monitoring/ajax/pc_modal', $pc_modal_data);
	}

	/**
	 * content -> ajax_content 영역
	 */
	public function ajax_view_content(){
		$case_by = TRIM($this->input->post('case_by', true));
		$pc_index = TRIM($this->input->post('pc_index', true));
		$group_index = TRIM($this->input->post('group_index', true));

		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);				//get user

		$corp_data = array('usernum'=>$user->c_index);
		$corp = $this->neturo_user_model->getCorpOnce($corp_data);
		$data['corp'] = $corp;													//set corp
		
		$data['current_pc'] = null;
		$data['current_group'] = null;

		/**
		 * no ajax -> ajax init -> case_by : root
		 */
		if($case_by == null || $case_by == '')	$case_by = 'root';				//init case_by
		
		/**
		 * ajax - root click
		 */
		if($case_by == 'root'){
			$root_data = array('u_index' => $user->index, 'isRoot' => 1);		
			$root = $this->pcany_group_model->getGroupOnce($root_data);			//root data get

			$group_index = $root->index;										//init group_index
			$pc_index = '';														//init pc_index

			$data['current_group'] = $root;

			$root_connect_data = array('g_index' => $group_index);													
			$root_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($root_connect_data);	//get connect of root

			$root_pc_list = array();
			foreach($root_connected['pc_connect'] as $root_pc_connect){
				$root_pc_data = array('servernum' => $root_pc_connect->p_index);					
				$root_pc = $this->neturo_server_info_model->getPcOnce($root_pc_data);								//get pc of root
				array_push($root_pc_list, $root_pc);
			}
			$data['pc'] = $root_pc_list;
			
			$group_data = array('u_index' => $user->index, 'isRoot' => 0);
			$data['belong'] = $this->pcany_group_model->getGroup($group_data);										//get group
		}
		
		/**
		 * ajax - group click
		 */
		if($case_by == 'group'){
			$group_data = array('index' => $group_index);
			$group = $this->pcany_group_model->getGroupOnce($group_data);											//get group

			$data['current_group'] = $group;

			$group_connect_data = array('g_index' => $group_index);														
			$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);	//get connect of group

			$group_pc_list = array();
			foreach($group_connected['pc_connect'] as $group_pc_connect){
				$group_pc_data = array('servernum' => $group_pc_connect->p_index);
				$group_pc = $this->neturo_server_info_model->getPcOnce($group_pc_data);								//get pc
				array_push($group_pc_list, $group_pc);
			}
			$data['pc'] = $group_pc_list;

			$data['belong'] = null;																					//set belong group
		}

		/**
		 * ajax - pc click
		 */
		if($case_by == 'pc'){
			$group_data = array('index' => $group_index);
			$group = $this->pcany_group_model->getGroupOnce($group_data);											//get group

			$data['current_group'] = $group;

			$pc_data = array('servernum' => $pc_index);
			$pc = $this->neturo_server_info_model->getPcOnce($pc_data);												//get pc
			$data['current_pc'] = $pc;

			$group_connect_data = array('g_index' => $group_index);														
			$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);	//get connect of group

			$group_pc_list = array();
			foreach($group_connected['pc_connect'] as $group_pc_connect){
				$group_pc_data = array('servernum' => $group_pc_connect->p_index);
				$group_pc = $this->neturo_server_info_model->getPcOnce($group_pc_data);								//get pc
				array_push($group_pc_list, $group_pc);
			}
			$data['pc'] = $group_pc_list;

			$group_data = array('u_index' => $user->index, 'isRoot' => 0);
			$data['belong'] = $this->pcany_group_model->getGroup($group_data);										//get group
		}

		/**
		 * ajax - belong pc click
		 */
		if($case_by == 'belong_pc'){
			$group_data = array('index' => $group_index);
			$group = $this->pcany_group_model->getGroupOnce($group_data);											//get group

			$data['current_group'] = $group;

			$pc_data = array('servernum' => $pc_index);
			$pc = $this->neturo_server_info_model->getPcOnce($pc_data);												//get pc
			$data['current_pc'] = $pc;

			$group_connect_data = array('g_index' => $group_index);														
			$group_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($group_connect_data);	//get connect of group

			$group_pc_list = array();
			foreach($group_connected['pc_connect'] as $group_pc_connect){
				$group_pc_data = array('servernum' => $group_pc_connect->p_index);
				$group_pc = $this->neturo_server_info_model->getPcOnce($group_pc_data);								//get pc
				array_push($group_pc_list, $group_pc);
			}
			$data['pc'] = $group_pc_list;

			$data['belong'] = null;																					//get group
		}

		$this->load->view('monitoring/ajax/content', $data);
	}
	/**
	 * content 영역
	 */
	public function view_content(){
		$this->load->view('monitoring/content');
	}

	/**
	 * side 영역
	 */
	public function view_side(){
		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);			//get current user

		$root_data = array('u_index' => $user->index, 'isRoot' => 1);
		$data['root'] = $this->pcany_group_model->getGroupOnce($root_data);	//get root of user

		$root_connect_data = array('g_index' => $data['root']->index);
		$root_connected['pc_connect'] = $this->pcany_group_pc_connect_model->getConnect($root_connect_data);//get connect
		
		$root_pc_list = array();											
		foreach($root_connected['pc_connect'] as $root_pc_connect){
			$root_pc_data = array('servernum' => $root_pc_connect->p_index);
			$root_pc = $this->neturo_server_info_model->getPcOnce($root_pc_data);
			array_push($root_pc_list, $root_pc);
		}

		$data['root_pc'] = $root_pc_list;									//get pc of root

		$group_data = array('u_index' => $user->index, 'isRoot' => 0);
		$data['belong'] = $this->pcany_group_model->getGroup($group_data);	//get etc group
		
		$this->load->view('monitoring/side', $data);							//send & view							
	}

	/**
	 * 주메뉴
	 */
	public function view_gnb(){
		$pc_manage_menu = "";
		$monitoring_menu = "active";
		$stats_menu = "";
		$conf_menu = "";
		$user_manage_menu = "";

		$header_param = array('isAdmin' => $this->session->userdata('isAdmin'), 
							'pc_manage' => $pc_manage_menu,
							'monitoring' => $monitoring_menu,
							'stats' => $stats_menu,
							'conf' => $conf_menu,
							'user_manage' => $user_manage_menu,
							'isLogin' => $this->session->userdata('isLogin')
							);

		$this->load->view('header', $header_param);
	}

	/**
	 * 그룹 생성
	 */
	public function create_group(){
		$group_name = TRIM($this->input->post('create_group_name', true));

		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);								//get user

		$group_data = array('u_index' => $user->index, 'name' => $group_name, 'isRoot' => 0);	
		$this->pcany_group_model->setGroup($group_data);										//set group
		
		$group = $this->pcany_group_model->getGroupOnce($group_data);							//get group

		$cookie = array('name' => $group->index, 'value' => "open", 'expire' => 0);				
		$this->input->set_cookie($cookie);														//set cookie

		redirect('monitoring');
	}
	
	/**
	 * 그룹 삭제
	 */
	public function delete_group(){
		$group_name = TRIM($this->input->post('delete_group_name', true));

		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);									//get user

		$group_data = array('u_index' => $user->index, 'name' => $group_name, 'isRoot' => 0);	
		$root_data = array('u_index' => $user->index, 'isRoot' => 1);							

		$group = $this->pcany_group_model->getGroupOnce($group_data);								//get group
		$root = $this->pcany_group_model->getGroupOnce($root_data);									//get root

		$this->pcany_group_model->delGroup($group_data);											//del group

		$group_pc_connect = array('g_index' => $group->index);									
		$group_pc['connect'] = $this->pcany_group_pc_connect_model->getConnect($group_pc_connect);	//get connect
		foreach($group_pc['connect'] as $connected){
			$init_connect_data = array('g_index' => $root->index, 'p_index'=>$connected->p_index);	
			$this->pcany_group_pc_connect_model->setConnect($init_connect_data);					//init connect of root
			$del_connect_data = array('index' => $connected->index);								
			$this->pcany_group_pc_connect_model->delConnect($del_connect_data);						//del connect of group
		}

		redirect('monitoring');
	}
	
	/**
	 * 그룹 이동
	 */
	public function move_group(){
		$bef_group_name = TRIM($this->input->post('bef_group', true));											//before Group
		$aft_group_name = TRIM($this->input->post('aft_group', true));											//after Group
		$sel_pc_name =  TRIM($this->input->post('sel_pc', true));												//selected PC
		
		$user_data = array('id'=>$this->session->userdata('id'));
		$user = $this->pcany_member_model->getUserOnce($user_data);												//get user
		
		$bef_group_data = array('u_index' => $user->index, 'name' => $bef_group_name);			
		$bef_group = $this->pcany_group_model->getGroupOnce($bef_group_data);									//get before group

		$pc_data = array('com_name' => $sel_pc_name);		
		$sel_pc = $this->neturo_server_info_model->getPcOnce($pc_data);											//get pc

		$bef_group_pc_connect_data = array('g_index' => $bef_group->index, 'p_index' => $sel_pc->servernum);	
		$this->pcany_group_pc_connect_model->delConnect($bef_group_pc_connect_data);							//del connect

		$aft_group_data = array('u_index' => $user->index, 'name' => $aft_group_name);						
		$aft_group = $this->pcany_group_model->getGroupOnce($aft_group_data);									//get after group

		$aft_group_pc_connect_data = array('g_index' => $aft_group->index, 'p_index' => $sel_pc->servernum);	
		$this->pcany_group_pc_connect_model->setConnect($aft_group_pc_connect_data);							//set connect

		redirect('monitoring');
	}

	/**
	 * 새로 고침
	 */
	public function reload(){
		redirect('monitoring');
	}
}
?>