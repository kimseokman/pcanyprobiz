<?PHP if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * index controller
 **
 * desc
 **
 * index()		- 기본 뷰
 * view_gnb()	- 주메뉴 뷰
 * login()		- 로그인 기능
 * logout()		- 로그아웃 기능
 *
 * 2014-07-23
 * by KSM
 */

class Index extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	public function index(){
		if($this->session->userdata('isLogin')){
			redirect('pc_manage');	//defualt
		}else{
			$file_name = "index";
			$title = "로그인 페이지";

			$this->load->view('html_head', array('file_name' => $file_name, 'title' => $title));
			$this->view_gnb();
			$this->load->view('index/content');
			$this->load->view('footer');
			$this->load->view('html_foot', array('file_name' => $file_name));
		}
	}

	/**
	 * 주메뉴
	 */
	public function view_gnb(){
		$pc_manage_menu = "";
		$monitoring_menu = "";
		$stats_menu = "";
		$conf_menu = "";
		$user_manage_menu = "";

		$header_param = array('isAdmin' => $this->session->userdata('isAdmin'), 
							'pc_manage' => $pc_manage_menu,
							'monitoring' => $monitoring_menu,
							'stats' => $stats_menu,
							'conf' => $conf_menu,
							'user_manage' => $user_manage_menu,
							'isLogin' => $this->session->userdata('isLogin')
							);

		$this->load->view('header', $header_param);
	}

	/**
	 * 로그인 기능
	 */
	public function login(){
		$this->load->model('pcany_member_model');

		$userID = TRIM($this->input->post('userID', true));
		$userPW = MD5(TRIM($this->input->post('userPW', true)));

		$user_data = array('id'=>$userID, 'pw'=>$userPW);

		$user = $this->pcany_member_model->getUserOnce($user_data);

		if($user != null){
			$this->session->set_userdata('isLogin', true);
			$this->session->set_userdata('id', $user->id);
			$this->session->set_userdata('isAdmin', $user->isAdmin);
		}else{
			$this->session->set_userdata('isLogin', false);
		}
		
		redirect('');
	}
	
	/**
	 * 로그아웃 기능
	 */
	public function logout(){
		$this->session->unset_userdata('id');
		$this->session->set_userdata('isLogin', false);
		redirect('');
	}
}
?>