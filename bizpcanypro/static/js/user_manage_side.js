$(document).ready(function(){
	/**
	 * pc drop event
	 */
	$(".droppable").droppable({
		greedy: true,
		hoverClass: "drag_hover",
		drop: function( event, ui ) {
			$('.aft_user').val($(this).children(':first-child').text());			//추가해야 될 부분
			$('.aft_user_name').text($('.aft_user').val());

			if($('.bef_user').val() != $('.aft_user').val()){						//사용자 id가 같지 않을 때
				$('.copy_pc').modal('show');
			}
		}
	});

	/**
	 * left click event
	 */
	$('.tree li a').on('click',function(e){
		e.preventDefault();

		$('.tree li a').removeClass('active');
		$(this).addClass('active');

		if($(this).parent().is(".admin") || $(this).parent().is(".auth_pc")){	//pc 선택시 그룹 삭제 버튼 disable
			$('.del_user_btn').addClass("disabled");
		}else{
			$('.del_user_btn').removeClass("disabled");
		}
	});

	$('.tree').on('click', 'a', function(e){
		e.preventDefault();

		sel_li = $(this).parent();
		sel_user_id = sel_li.children('input.user_id').val();
		user_index = '';

		$('#select_user').val(sel_user_id);		//삭제할 사용자 id html에 추가 php로 넘김
		$('.select_user_id').text(sel_user_id);	//삭제할 사용자 id html에 추가 사용자에게 보여줌

		if(sel_li.is('.admin')){
			user_index = sel_li.children('input.admin_index').val();
		}else if(sel_li.is('.user')){
			user_index = sel_li.children('input.user_index').val();
		}else if(sel_li.is('.auth_pc')){
			user_index = sel_li.children('input.auth_user_index').val();
		}

		before_load();

		$.ajax({
			url : "/bizpcanypro/index.php/user_manage/ajax_view_content",
			type: "POST",
			data:{
				"user_index": user_index
			},
			dataType: "html",
			complete: function(xhr, textStatus){
				if(textStatus == 'success'){
					$("#ajax_content").html(xhr.responseText);
					after_load();
				}
			}
		});
	});

	/**
	 * double click event
	 */
	$('.tree li.user a').on('dblclick',function(e){
		e.preventDefault();

		var a_tag = $(this);
		var li_tag = $(this).parent();
		var ul_tag = $(this).parent().parent();

		var user_index = li_tag.find('.user_index').val(); //group cookie

		if(li_tag.find('.isFold').val() == 'true'){		//유저 하위가 접혀 있으면
			a_tag.find('i.fa-caret-right').attr('class', 'fa fa-caret-down');
			ul_tag.children('li.auth_pc').show();
			li_tag.find('.isFold').val('false');		//열기
			$.cookie(user_index, "open", {expire: 0});
		}else{											//유저 하위가 열려 있으면
			a_tag.find('i.fa-caret-down').attr('class', 'fa fa-caret-right');
			ul_tag.children('li.auth_pc').hide();
			li_tag.find('.isFold').val('true');			//접기
			$.cookie(user_index, "close", {expire: 0});
		}
	});

	/**
	 * first loading compulsion event
	 */
	$('.tree li.admin a').trigger('click');

	/**
	 * right click event
	 */
	admin_menu_user = $('#admin_menu_user');
	admin_menu_pc = $('#admin_menu_pc');
	admin_click = false;
	belong_user_click = false;
	belong_pc_click = false;

	//menu 분기 설정
	$(".tree li a").bind("contextmenu", function(e){
		sel_li = $(this).parent();

		if(sel_li.is('.admin')){
			admin_click = true;
			belong_user_click = false;
			belong_pc_click = false;
		}else if(sel_li.is('.user')){
			admin_click = false;
			belong_user_click = true;
			belong_pc_click = false;

			var sel_user_id = sel_li.children('input.user_id').val();

			$('#select_user').val(sel_user_id);		//삭제할 사용자 id html에 추가 php로 넘김
			$('.select_user_id').text(sel_user_id);	//삭제할 사용자 id html에 추가 사용자에게 보여줌
		}else if(sel_li.is('.auth_pc')){
			admin_click = false;
			belong_user_click = false;
			belong_pc_click = true;
	
			$('#belong_to_user').val(sel_li.children('input.auth_user_index').val());	//해당 pc의 상위 권한자 index
			$('#select_pc_index').val(sel_li.children('input.auth_pc_index').val());	//삭제할 pc index html에 추가 php로 넘김
			$('.sel_pc_name').text(sel_li.children('input.auth_pc_name').val());		//삭제할 pc 이름 html에 추가 사용자에게 보여줌
			$('.auth_user_id').text(sel_li.children('input.auth_user_id').val());		//삭제할 pc의 상위 권한자 이름 html에 추가 사용자에게 보여줌
		}
	});

	//menu 영역 설정
	$('.side').bind("contextmenu",function(e){
		if(belong_user_click){
			$(".user_del").parent().removeClass('disabled');
		}else{
			$(".user_del").parent().addClass('disabled');
		}

		if(belong_pc_click){
			admin_menu_pc.css({
				display:"block",
				left:e.clientX,
				top:e.clientY
			});
		}else{
			admin_menu_user.css({
				display:"block",
				left:e.clientX,
				top:e.clientY
			});
		}
		
		return false;
	});

	//admin_menu_user event
	admin_menu_user.on("click",".user_add",function(e){
		$('.create_user').modal('show');
	});

	admin_menu_user.on("click",".user_del",function(e){
		$('.delete_user').modal('show');
	});
	
	//admin_menu_pc event
	admin_menu_pc.on("click",".pc_del",function(e){
		$('.remove_pc').modal('show');
	});


	$("#container").mousedown(function(e){
		//admin_menu 닫기
		if(admin_menu_user.has(e.target).length==0){//없으면 menu on 이벤트 먹통
			admin_menu_user.hide();
			belong_user_click = false;
			admin_click = false;
			belong_pc_click = false;
		}

		if(admin_menu_pc.has(e.target).length==0){//없으면 menu on 이벤트 먹통
			admin_menu_pc.hide();
			belong_user_click = false;
			admin_click = false;
			belong_pc_click = false;
		}
	});
});

var before_load = function(){
	$('.preloader').show();
	$('#ajax_content').hide();
}

var after_load = function(){
	$('.preloader').hide();
	$('#ajax_content').show();
}