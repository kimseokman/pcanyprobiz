$(document).ready(function(){
	/**
	 * top menu event
	 */
	$('.show_side').on('click', function(e){
		e.preventDefault();
		$(this).toggleClass('active');
	});

	$('.dropdown a').on('click', function(e){
		e.preventDefault();
		$(this).toggleClass('active');
	});
	
	$('.dropdown a').on('focusout', function(){
		$('.dropdown a').attr('class','');//all class init
	});

	/**
	 * top menu <-> side bar event
	 */
	
	$('.show_side').on('click', function(e){
		e.preventDefault();
		$('#container').toggleClass('side-show');
	});

	$('.logout').on('click', function(e){
		e.preventDefault();
		location.href="index/logout";
	});

	/**
	 * first loading compulsion event
	 */
	$('.show_side').trigger('click');
});