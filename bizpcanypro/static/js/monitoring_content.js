$(document).ready(function(){ 
	/**
	 * click event
	 */
	$(".list_view li a").on('click',function(e){
		e.preventDefault();

		$('.list_view li a').removeClass('on');
		$(this).addClass('on');
	});

	/**
	 * double click event
	 */
	$(".list_view li.lg_group a").on('dblclick',function(e){
		e.preventDefault();

		li_tag = $(this).parent();
		case_by = 'group';
		pc_index = "";
		group_index = li_tag.find('input.group_index').val();				//현재 선택된 그룹 index
		
		before_load();

		$.ajax({
			url : "/bizpcanypro/index.php/monitoring/ajax_view_content",
			type: "POST",
			data:{
				"pc_index" : pc_index,
				"group_index" : group_index,
				"case_by" : case_by
			},
			dataType: "html",
			complete: function(xhr, textStatus){
				if(textStatus == 'success'){
					$("#ajax_content").html(xhr.responseText);
					after_load();
				}
			}
		});
	});

	$(".list_view li.lg_pc a").on('dblclick',function(e){
		e.preventDefault();

		pc_index = $(this).parent().find('input.pc_index').val();
		corp_id = $(this).parent().find('input.corp_id').val();

		before_load();

		$.ajax({
			url : "/bizpcanypro/index.php/monitoring/ajax_view_content_modal",
			type: "POST",
			data:{
				"pc_index" : pc_index,
				"corp_id" : corp_id
			},
			dataType: "html",
			complete: function(xhr, textStatus){
				if(textStatus == 'success'){
					$(".ajax_pc_modal").html(xhr.responseText);
					after_load();
					$(".modal_pc").modal('show');
				}
			}
		});
	});

	var imgRefresh = setInterval(function() {
		$('img.refleshImg').each(function() {
			var time = (new Date()).getTime();
			var oldurl = $(this).prop('src');
			var newurl = oldurl.substring(0, oldurl.lastIndexOf('?'))+ "?date=" + time.toString().substr(7, 6);
			//var newurl = oldurl + "?date=" + time;
			//alert(newurl);
			$('#content').html();
			$(this).prop('src', newurl);
			//$(this).prop('src', oldurl);
		});
	}, 10000);
});

var imgRefreshOff = function(imgRefresh){
	clearInterval(imgRefresh);
}

var before_load = function(){
	$('.preloader').show();
	$('#ajax_content').hide();
}

var after_load = function(){
	$('.preloader').hide();
	$('#ajax_content').show();
}