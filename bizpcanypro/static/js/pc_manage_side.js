$(document).ready(function(){
	/**
	 * pc drag event
	 */
	$(".draggable").draggable({
		drag: function( event, ui ) {
			$('.bef_group').val($(this).parent().children(':first-child').text());	//삭제 해야할 부분
			$('.bef_group_name').text($('.bef_group').val());
			$('.sel_pc').val($(this).text());										//삭제 해야할 데이터
			$('.sel_pc_name').text($('.sel_pc').val());								//삭제 해야할 PC 이름 표시
		},
		revert: true
	});

	/**
	 * pc drop event
	 */
	$(".droppable").droppable({
		greedy: true,
		hoverClass: "drag_hover",
		drop: function( event, ui ) {
			$('.aft_group').val($(this).children(':first-child').text());			//추가해야 될 부분
			$('.aft_group_name').text($('.aft_group').val());

			if($('.bef_group').val() != $('.aft_group').val()){						//그룹 이름이 같지 않을 때
				$('.move_group').modal('show');
			}
		}
	});

	/**
	 * left click event
	 */
	$('.tree li a').on('click',function(e){
		e.preventDefault();

		$('.tree li a').removeClass('active');
		$(this).addClass('active');

		if($(this).parent().is(".root") || $(this).parent().is(".belong_pc") || $(this).parent().is(".pc")){	//pc 선택시 그룹 삭제 버튼 disable
			$('.del_group_btn').addClass("disabled");
		}else{
			$('.del_group_btn').removeClass("disabled");
		}
	});

	$('.tree').on('click', 'a', function(e){
		e.preventDefault();

		var li_tag = $(this).parent();
		var sel_group_name = "";
		var pc_index = "";
		var group_index = "";
		var case_by = 'root';
		
		if(li_tag.is('.root')){					//root 인 경우
			case_by = 'root';
			pc_index = "";
			group_index = li_tag.find('input.root_index').val();					//현재 선택된 그룹 index
		}else if(li_tag.is('.belong_root')){	//하위 그룹인 경우
			case_by = 'group';
			sel_group_name = li_tag.children('input.group_name').val();

			/**
			 * 그룹 삭제 할 때 사용
			 */
			$('#delete_group_name').val(sel_group_name);	//삭제할 그룹 이름 jquery -> html -> php 로 전송
			$('.delete_group_name').text(sel_group_name);	//삭제할 그룹 이름 표시
			
			/**
			 * ajax에 사용
			 */
			pc_index = "";
			group_index = li_tag.find('input.group_index').val();				//현재 선택된 그룹 index
		}else if(li_tag.is('.pc')){	//pc인 경우
			case_by = 'pc';

			/**
			 * ajax에 사용
			 */
			pc_index = li_tag.find('input.pc_index').val();						//현재 선택된 pc index
			group_index = li_tag.find('input.parent_group_index').val();
		}else if(li_tag.is('.belong_pc')){		//하위 pc인 경우
			case_by = 'belong_pc';

			/**
			 * ajax에 사용
			 */
			pc_index = li_tag.find('input.belong_pc_index').val();				//현재 선택된 pc index
			group_index = li_tag.find('input.parent_group_index').val();
		}

		before_load();

		$.ajax({
			url : "/bizpcanypro/index.php/pc_manage/ajax_view_content",
			type: "POST",
			data:{
				"pc_index" : pc_index,
				"group_index" : group_index,
				"case_by" : case_by
			},
			dataType: "html",
			complete: function(xhr, textStatus){
				if(textStatus == 'success'){
					$("#ajax_content").html(xhr.responseText);
					after_load();
				}
			}
		});
	});

	/**
	 * right click event
	 */
	root_menu = $("#root_menu");
	belong_pc_click = false;
	belong_group_click = false;
	belong_root_click = false;

	//menu 분기 설정
	$(".tree li a").bind("contextmenu", function(e){	
		if($(this).parent().children().is(".root_index")){
			belong_root_click = true;
			belong_group_click = false;
			belong_pc_click = false;
		}else if($(this).parent().children().is(".group_name")){	//그룹일때
			belong_root_click = false;
			belong_group_click = true;
			belong_pc_click = false;

			var select_group_name = $(this).parent().children('input.group_name').val();

			$('#delete_group_name').val(select_group_name);	//삭제할 그룹 이름 jquery -> html -> php 로 전송
			$('.delete_group_name').text(select_group_name);	//삭제할 그룹 이름 표시
		}else if($(this).parent().children().is(".pc_index") || $(this).parent().children().is(".belong_pc_index")){	//PC일때
			belong_root_click = false;
			belong_group_click = false;
			belong_pc_click = true;
		}
	});

	//menu 영역 설정
	$('.side').bind("contextmenu",function(e){
		if(belong_group_click){
			$(".group_del").parent().removeClass('disabled');
		}else{
			$(".group_del").parent().addClass('disabled');
		}

		if(belong_pc_click){
			//pc 메뉴
		}else{
			root_menu.css({
				display:"block",
				left:e.clientX,
				top:e.clientY
			});
		}
		
		return false;
	});

	//root_menu event
	root_menu.on("click",".group_add",function(e){
		$('.create_group').modal('show');
	});

	root_menu.on("click",".group_del",function(e){
		if(belong_group_click){
			$('.delete_group').modal('show');
		}
	});

	//root_menu 닫기
	$("#container").mousedown(function(e){
		if(root_menu.has(e.target).length==0){//없으면 menu on 이벤트 먹통
			root_menu.hide();
			belong_pc_click = false;
			belong_group_click = false;
			belong_root_click = false;
		}
	});

	/**
	 * double click event
	 */
	$('.tree li.belong_root a').on('dblclick',function(e){
		e.preventDefault();
		var a_tag = $(this);
		var li_tag = $(this).parent();
		var ul_tag = $(this).parent().parent();

		var group_index = li_tag.find('.group_index').val(); //group cookie

		if(li_tag.find('.isFold').val() == 'true'){		//폴더가 접혀 있으면
			a_tag.find('i').attr('class', 'fa fa-folder-open-o');
			ul_tag.children('li.belong_pc').show();
			li_tag.find('.isFold').val('false');		//열기
			$.cookie(group_index, 'open', {expire: 0});
		}else{											//폴더가 열려 있으면
			a_tag.find('i').attr('class', 'fa fa-folder-o');
			ul_tag.children('li.belong_pc').hide();
			li_tag.find('.isFold').val('true');			//접기
			$.cookie(group_index, 'close', {expire: 0});
		}
	});

	/**
	 * first loading compulsion event
	 */
	$('.tree li.root a').trigger('click');

});

var before_load = function(){
	$('.preloader').show();
	$('#ajax_content').hide();
}

var after_load = function(){
	$('.preloader').hide();
	$('#ajax_content').show();
}