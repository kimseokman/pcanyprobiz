$(document).ready(function(){
	/**
	 * left click event
	 */
	$('.stats_nav li a').on('click',function(e){
		e.preventDefault();

		$('.stats_nav li a').removeClass('active');
		$(this).addClass('active');
	});
});

var before_load = function(){
	$('.preloader').show();
	$('#ajax_content').hide();
}

var after_load = function(){
	$('.preloader').hide();
	$('#ajax_content').show();
}