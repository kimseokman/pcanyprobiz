$(document).ready(function(){
	/**
	 * pc drag event
	 */
	$(".lg_draggable").draggable({
		drag: function( event, ui ) {
			$('.bef_group').val($(".parent_group_name").val());			//삭제 해야할 부분
			$('.bef_group_name').text($('.bef_group').val());
			$('.sel_pc').val($(this).text());								//삭제 해야할 데이터
			$('.sel_pc_name').text($('.sel_pc').val());						//삭제 해야할 PC 이름 표시
		},
		revert: true
	});
	

	/**
	 * pc drop event
	 */
	$(".lg_droppable").droppable({
		greedy: true,
		hoverClass: "drag_hover",
		drop: function( event, ui ) {
			$('.aft_group').val($(this).text());			//추가해야 될 부분
			$('.aft_group_name').text($('.aft_group').val());

			if($('.bef_group').val() != $('.aft_group').val()){						//그룹 이름이 같지 않을 때
				$('.move_group').modal('show');
			}
		}
	});
	 

	/**
	 * click event
	 */
	$(".list_view li a").on('click',function(e){
		e.preventDefault();

		$('.list_view li a').removeClass('on');
		$(this).addClass('on');
	});

	/**
	 * double click event
	 */
	$(".list_view li.lg_group a").on('dblclick',function(e){
		e.preventDefault();

		li_tag = $(this).parent();
		case_by = 'group';
		pc_index = "";
		group_index = li_tag.find('input.group_index').val();				//현재 선택된 그룹 index
		
		before_load();

		$.ajax({
			url : "/bizpcanypro/index.php/pc_manage/ajax_view_content",
			type: "POST",
			data:{
				"pc_index" : pc_index,
				"group_index" : group_index,
				"case_by" : case_by
			},
			dataType: "html",
			complete: function(xhr, textStatus){
				if(textStatus == 'success'){
					$("#ajax_content").html(xhr.responseText);
					after_load();
				}
			}
		});
	});
});

var before_load = function(){
	$('.preloader').show();
	$('#ajax_content').hide();
}

var after_load = function(){
	$('.preloader').hide();
	$('#ajax_content').show();
}