$(document).ready(function(){ 
	/**
	 * click event
	 */
	$(".list_view li a").on('click',function(e){
		e.preventDefault();

		$('.list_view li a').removeClass('on');
		$(this).addClass('on');
	});

	/**
	 * pc drag event
	 * start, stop 은 content에서 pc를 드래그 시 content를 넘어 side로 넘어갈때
	 * 넘어가는 모습을 보여주기 위해 overflow visible을 잠시 사용
	 */
	$(".draggable").draggable({
		start: function( event, ui ) {
			$('#content').css('overflow','visible');
		},
		drag: function( event, ui ) {
			$('.sel_pc').val($(this).text());										//추가 해야할 데이터
			$('.sel_pc_name').text($('.sel_pc').val());								//추가 해야할 PC 이름 표시
		},
		stop: function( event, ui ) {
			$('#content').css('overflow','hidden');
		},
		revert: true,
	});
});

var before_load = function(){
	$('.preloader').show();
	$('#ajax_content').hide();
}

var after_load = function(){
	$('.preloader').hide();
	$('#ajax_content').show();
}