$(document).ready(function(){
	/**
	 * left click event
	 */
	$('.conf_nav li a').on('click',function(e){
		e.preventDefault();
		
		if(!$(this).parent().is('.has_sub') && !$(this).parent().parent().is('.sub_menu')){
			$('.sub_menu').hide();
		}

		$('.conf_nav li a').removeClass('active');
		$(this).addClass('active');
		
		if($(this).parent().is('.has_sub')){
			$(this).parent().find('ul.sub_menu').show();
		};
	});
});

var before_load = function(){
	$('.preloader').show();
	$('#ajax_content').hide();
}

var after_load = function(){
	$('.preloader').hide();
	$('#ajax_content').show();
}