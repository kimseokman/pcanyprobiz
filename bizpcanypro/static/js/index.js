$(document).ready(function(){
	$('.theme li.theme1 a').on('click',function(e){
		e.preventDefault();
		trans_theme('w');
	});
	$('.theme li.theme2 a').on('click',function(e){
		e.preventDefault();
		trans_theme('b');
	});
	$('.theme li.theme3 a').on('click',function(e){
		e.preventDefault();
		trans_theme('g');
	});
});

var trans_theme = function(version){
	if(version == 'w'){
		//배경
		$('body').css('background','#6AA6D6');
		$('body').css('background-image','url("/bizpcanypro/static/img/pattern_01.png")');
		//로고
		$('.logo').css('background','#525252');
		$('.logo').css('background-image','url("/bizpcanypro/static/img/pattern_01.png")');
		$('.logo a').css('background-image','url("/bizpcanypro/static/img/logo_w.png")');
		$('.logo a img').attr('src', '/bizpcanypro/static/img/txt_logo_w.png');
		//content
		$('.lg_logo').attr('src', '/bizpcanypro/static/img/lg_logo_w.png');
		$('.lg_txt_logo').attr('src', '/bizpcanypro/static/img/lg_txt_logo_w.png');
		//footer
		$('#footer a').css('color','#fff');
		$('#footer p').css('color','#fff');
	}else if(version == 'b'){
		//배경
		$('body').css('background','#f5f5f5');
		//로고
		$('.logo').css('background','#eaeaea');
		$('.logo a').css('background-image','url("/bizpcanypro/static/img/logo_b.png")');
		$('.logo a img').attr('src', '/bizpcanypro/static/img/txt_logo_b.png');
		//content
		$('.lg_logo').attr('src', '/bizpcanypro/static/img/lg_logo_b.png');
		$('.lg_txt_logo').attr('src', '/bizpcanypro/static/img/lg_txt_logo_b.png');
		//footer
		$('#footer a').css('color','#666');
		$('#footer p').css('color','#666');
	}else if(version == 'g'){
		//배경
		$('body').css('background','#f5f5f5');
		//로고
		$('.logo').css('background','#eaeaea');
		$('.logo a').css('background-image','url("/bizpcanypro/static/img/logo_g.png")');
		$('.logo a img').attr('src', '/bizpcanypro/static/img/txt_logo_g.png');
		//content
		$('.lg_logo').attr('src', '/bizpcanypro/static/img/lg_logo_g.png');
		$('.lg_txt_logo').attr('src', '/bizpcanypro/static/img/lg_txt_logo_g.png');
		//footer
		$('#footer a').css('color','#154058');
		$('#footer p').css('color','#154058');
	}
}